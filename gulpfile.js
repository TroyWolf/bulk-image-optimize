var gulp = require('gulp');
var smushit = require('gulp-smushit');

gulp.task('optimize', function () {
    return gulp.src('before/**/*.{jpg,png}')
        .pipe(smushit())
        .pipe(gulp.dest('after'));
});
