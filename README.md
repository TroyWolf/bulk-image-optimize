# Bulk Image Optimize

A Quick & Dirty helper to optimize images in bulk through the reSmush.it API.

http://resmush.it/

Uses the gulp-smushit package

https://www.npmjs.com/package/gulp-smushit


Place your "before" image files in the `before` directory.

Kick off the procecsing from cmdline:

`gulp optimize`

The optimized files will land in the `after` directory.
